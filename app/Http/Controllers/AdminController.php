<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Session;
use App\Order;
use App\User;
use App\Product;
use App\Category;
use App\OrdersProduct;
use App\Admin;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function login(Request $request){
    	if($request->isMethod('post')){
    		$data = $request->input();
            $adminCount = Admin::where(['username' => $data['username'],'password'=>md5($data['password']),'status'=>1])->count(); 
            if($adminCount > 0){
                //echo "Success"; die;
                Session::put('adminSession', $data['username']);
                return redirect('/admin/dashboard');
        	}else{
                //echo "failed"; die;
                return redirect('/admin')->with('flash_message_error','Neispravno korisničko ime ili lozinka');
        	}
    	}
    	return view('admin.admin_login');
    }

    public function dashboard(){
        // Orders 
        $orders = Order::with('orders')->orderBy('id','ASC')->get();
        $ordersCount = Order::with('orders')->count();
        $orders = json_decode(json_encode($orders));
        $ordersCount = json_decode(json_encode($ordersCount));
          
           // User count
           $userCount = User::with('users')->count();
           $userCount = json_decode(json_encode($userCount));
  // Products  
  $productsCount = Product::with('product')->count();
  $productsCount = json_decode(json_encode($productsCount));
            // Categories
            $categoriesCount = Category::with('categories')->count();
            $categoriesCount = json_decode(json_encode($categoriesCount));
  
         
        /*if(Session::has('adminSession')){
            // Perform all actions
        }else{
            //return redirect()->action('AdminController@login')->with('flash_message_error', 'Please Login');
            return redirect('/admin')->with('flash_message_error','Please Login');
        }*/
        return view('admin.dashboard')->with(compact('orders','ordersCount','productsCount', 'userCount', 'categoriesCount'));
    }

    public function settings(){

        $adminDetails = Admin::where(['username'=>Session::get('adminSession')])->first();

        //echo "<pre>"; print_r($adminDetails); die;

        return view('admin.settings')->with(compact('adminDetails'));
    }

    public function chkPassword(Request $request){
        $data = $request->all();
        //echo "<pre>"; print_r($data); die;
        $adminCount = Admin::where(['username' => Session::get('adminSession'),'password'=>md5($data['current_pwd'])])->count(); 
            if ($adminCount == 1) {
                //echo '{"valid":true}';die;
                echo "true"; die;
            } else {
                //echo '{"valid":false}';die;
                echo "false"; die;
            }

    }

    public function updatePassword(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>"; print_r($data); die;
            $adminCount = Admin::where(['username' => Session::get('adminSession'),'password'=>md5($data['current_pwd'])])->count();

            if ($adminCount == 1) {
                // here you know data is valid
                $password = md5($data['new_pwd']);
                Admin::where('username',Session::get('adminSession'))->update(['password'=>$password]);
                return redirect('/admin/settings')->with('flash_message_success', 'Zaporka je uspješno ažurirana.');
            }else{
                return redirect('/admin/settings')->with('flash_message_error', 'Trenutačna unesena lozinka nije točna.');
            }

            
        }
    }

    public function logout(){
        Session::flush();
        return redirect('/admin')->with('flash_message_success', 'Uspješno odjavljen.');
       
    }
}
