<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
class CmsController extends Controller
{
  

    public function contact(Request $request){
           if($request->isMethod('post')){
                $data = $request->all();

                $email = "info@katrieldev.com";
                $messageData = [
                    'name'=>$data['name'],
                    'email'=>$data['email'],
                    'subject'=>$data['subject'],
                    'comment'=>$data['message']
                ];

                Mail::send('emails.enquery', $messageData, function($message)use($email){
                     $message->to($email)->subject('Upit od korisnika');

                });

                return redirect()->back()->with('flash_message_success', 'Zahvaljujemo na zahtjevu. Uskoro ćemo Vas kontaktirati.');
           }
        return view('pages.kontakt');
    }

     
}
