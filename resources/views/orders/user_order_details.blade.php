@extends('layouts.frontLayout.front_design')
@section('content')

<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
				<div class="container">
					<div class="breadcrumb-content text-center">
						<ul>
							<li>
								<a href="index.html">Početna</a>
							</li>
							<li class="active">Naručeni proizvodi </li>
						</ul>
					</div>
                </div>
</div>
<div class="py-5"></div>
<section id="do_action">
	<div class="container">
		<div class="heading" align="center">
			<table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Kod proizvoda</th>
                        <th>Naziv </th>
                        <th>Veličina</th>
                        <th>Boja</th>
                        <th>Cijena</th>
                        <th>Količina</th>
                    </tr>
                </thead>
                <tbody>
                	@foreach($orderDetails->orders as $pro)
                    <tr>
                        <td>{{ $pro->product_code }}</td>
                        <td>{{ $pro->product_name }}</td>
                        <td>{{ $pro->product_size }}</td>
                        <td>{{ $pro->product_color }}</td>
                        <td>{{ $pro->product_price }}</td>
                        <td>{{ $pro->product_qty }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
		</div>
	</div>
</section>
<div class="py-5"></div>
@endsection