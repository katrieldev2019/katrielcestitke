@extends('layouts.frontLayout.front_design')
@section('content')

<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
				<div class="container">
					<div class="breadcrumb-content text-center">
						<ul>
							<li>
								<a href="index.html">Početna</a>
							</li>
							<li class="active">Vaše narudžbe </li>
						</ul>
					</div>
                </div>
</div>
<div class="py-5"></div>
<section id="do_action">
	<div class="container">
		<div class="heading" align="center">
			<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID Narudžbe</th>
                <th>Naručeni proizvodi</th>
                <th>Metod plaćanja</th>
                <th>Ukupno</th>
                <th>Kreirano</th>
            </tr>
        </thead>
        <tbody>
        	@foreach($orders as $order)
            <tr>
                <td>{{ $order->id }}</td>
                <td>
                	@foreach($order->orders as $pro)
                		<a href="{{ url('/orders/'.$order->id) }}">{{ $pro->product_code }}</a><br>
                	@endforeach
                </td>
                <td>{{ $order->payment_method }}</td>
                <td>{{ $order->grand_total }}</td>
                <td>{{ $order->created_at }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
		</div>
	</div>
</section>
<div class="py-5"></div>
@endsection