@extends('layouts.frontLayout.front_design')
@section('content')

<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
				<div class="container">
					<div class="breadcrumb-content text-center">
						<ul>
							<li>
								<a href="index.html">Početna</a>
							</li>
							<li class="active">Narudžba </li>
						</ul>
					</div>
				</div>
</div>
<section id="do_action">
	<div class="container">
		<div class="heading" align="center">
			<h3>Vaša narudžba je uspješna</h3>
			<p>Broj vaše narudžbe {{ Session::get('order_id') }} totalni iznos KM {{ Session::get('grand_total') }}</p>
		</div>
	</div>
</section>
<div class="py-5"></div>
<div class="py-5"></div>
<div class="py-5"></div>
@endsection

<?php
Session::forget('grand_total');
Session::forget('order_id');
?>