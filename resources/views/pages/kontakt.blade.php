@extends('layouts.frontLayout.front_design')

@section('content')
<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <ul>
                <li>
                    <a href="index.html">Početna</a>
                </li>
                <li class="active">Kontakt</li>
            </ul>
        </div>
    </div>
</div>
<div class="contact-area pt-100 pb-100">
    <div class="container">
            <div class="custom-row-2">
                    <div class="col-lg-4 col-md-5">
                        <div class="contact-info-wrap">
                            <div class="single-contact-info">
                                <div class="contact-icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="contact-info-dec">
                                    <p>+ 387 (0)30 708 000</p>
                                </div>
                            </div>
                            <div class="single-contact-info">
                                <div class="contact-icon">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="contact-info-dec">
                                    <p><a href="#">info@katriel.ba</a></p>
                                    <p><a href="#">katriel.ba</a></p>
                                </div>
                            </div>
                            <div class="single-contact-info">
                                <div class="contact-icon">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <div class="contact-info-dec">
                                    <p>Donje Putićevo b.b., </p>
                                    <p>Nova Bila, Bosna i Hercegovina</p>
                                </div>
                            </div>
                            <div class="contact-social text-center">
                                <h3>Pratite nas</h3>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                  
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="contact-form">
                            <div class="contact-title mb-30">
                                <h2>Stupite u kontakt</h2>
                            </div>
                            @if(Session::has('flash_message_success'))
	            <div class="alert alert-success alert-block">
	                <button type="button" class="close" data-dismiss="alert">×</button> 
	                    <strong>{!! session('flash_message_success') !!}</strong>
	            </div>
	        @endif
            <form action="{{url('/pages/kontakt')}}" method="post" name="contact-form"  id="main-contact-form">{{csrf_field()}}
                 <div class="row">
                    <div class="form-group col-md-6">
                        <input type="text" name="name" class="form-control" required="" placeholder="Ime i prezime">
                    </div>
                    <div class="form-group col-md-6">
                         <input type="email" name="email" class="form-control" required="" placeholder="Email adresa">
                    </div>

                 </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <input type="text" name="subject" class="form-control" required="" placeholder="Subjekt">
                    </div>
                    <div class="form-group col-md-12">
                        <textarea name="message" id="message" required="" class="form-control" rows="8" placeholder="Poruka"></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <input type="submit" name="submit" class="btn btn-primary pull-right" value="Senden">
                    </div>
                
                </div>
              
          </form>
                            
                        </div>
                    </div>
                </div>
       
       
    </div>
</div>

@endsection