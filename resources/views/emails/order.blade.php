<html>
<body>
	<table width='700px'>
		<tr><td>&nbsp;</td></tr>
		<tr><td><img src="{{ asset('images/frontend_images/home/logo.png') }}"></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>Zdravo {{ $name }},</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>Hvala vam što kupujete s nama. Pojedinosti o narudžbi prikazane su u nastavku:-</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>Broj narudžbe: {{ $order_id }}</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>
			<table width='95%' cellpadding="5" cellspacing="5" bgcolor="#f7f4f4">
				<tr bgcolor="#cccccc">
					<td>Naziv proizvoda</td>
					<td>Kod proizvoda</td>
					
					
					<td>Količina</td>
					<td>Cijena po proizvodu</td>
				</tr>
				@foreach($productDetails['orders'] as $product)
					<tr>
						<td>{{ $product['product_name'] }}</td>
						<td>{{ $product['product_code'] }}</td>
					
						
						<td>{{ $product['product_qty'] }}</td>
						<td> {{ $product['product_price'] }} KM</td>
					</tr>
				@endforeach
				
				<tr>
					<td colspan="5" align="right">Kupon popust</td><td>{{ $productDetails['coupon_amount'] }} KM</td>
				</tr>
				<tr>
					<td colspan="5" align="right">Ukupno</td><td> {{ $productDetails['grand_total'] }} KM</td>
				</tr>
			</table>
		</td></tr>
		<tr><td>
			<table width="100%">
				<tr>
					<td width="50%">
						<table>
							<tr>
								<td><strong>Podaci za račun :-</strong></td>
							</tr>
							<tr>
								<td>{{ $userDetails['name'] }}</td>
							</tr>
							<tr>
								<td>{{ $userDetails['address'] }}</td>
							</tr>
							<tr>
								<td>{{ $userDetails['city'] }}</td>
							</tr>
							<tr>
								<td>{{ $userDetails['state'] }}</td>
							</tr>
							<tr>
								<td>{{ $userDetails['country'] }}</td>
							</tr>
							<tr>
								<td>{{ $userDetails['pincode'] }}</td>
							</tr>
							<tr>
								<td>{{ $userDetails['mobile'] }}</td>
							</tr>
						</table>
					</td>
					<td width="50%">
						<table>
							<tr>
								<td><strong>Podaci za dostavu :-</strong></td>
							</tr>
							<tr>
								<td>{{ $productDetails['name'] }}</td>
							</tr>
							<tr>
								<td>{{ $productDetails['address'] }}</td>
							</tr>
							<tr>
								<td>{{ $productDetails['city'] }}</td>
							</tr>
							<tr>
								<td>{{ $productDetails['state'] }}</td>
							</tr>
							<tr>
								<td>{{ $productDetails['country'] }}</td>
							</tr>
							<tr>
								<td>{{ $productDetails['pincode'] }}</td>
							</tr>
							<tr>
								<td>{{ $productDetails['mobile'] }}</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>Za bilo kakva pitanja, možete nas kontaktirati na <a href="mailto:info@ecom-website.com">info@ecom-website.com</a></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>Pozdrav,<br> Katriel Tim</td></tr>
		<tr><td>&nbsp;</td></tr>
	</table>
</body>
</html>