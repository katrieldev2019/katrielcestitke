<?php $url = url()->current(); ?>
<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li <?php if (preg_match("/dashboard/i", $url)){ ?> class="active" <?php } ?>><a href="{{ url('/admin/dashboard') }}"><i class="icon icon-home"></i> <span id="sidebarNav">Dashboard</span></a> </li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Kategorije</span> 
      <ul <?php if (preg_match("/categor/i", $url)){ ?> style="display: block;" <?php } ?>>
        <li <?php if (preg_match("/add-category/i", $url)){ ?> class="active" <?php } ?>><a href="{{ url('/admin/add-category')}}"><span id="sidebarNav">Dodaj novu kategoriju</span></a></li>
        <li <?php if (preg_match("/view-categories/i", $url)){ ?> class="active" <?php } ?>><a href="{{ url('/admin/view-categories')}}"><span id="sidebarNav">Lista kategorija</span></a></li>
      </ul>
    </li>
     <li class="submenu"> <a href="#"><i class="fa fa-product-hunt" aria-hidden="true"></i>
 <span>Proizvodi</span>
      <ul <?php if (preg_match("/product/i", $url)){ ?> style="display: block;" <?php } ?>>
        <li <?php if (preg_match("/add-product/i", $url)){ ?> class="active" <?php } ?>><a href="{{ url('/admin/add-product')}}"><span id="sidebarNav">Dodaj novi proizvod</span></a></li>
        <li <?php if (preg_match("/view-products/i", $url)){ ?> class="active" <?php } ?>><a href="{{ url('/admin/view-products')}}"><span id="sidebarNav">Lista proizvoda</span></a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="fa fa-gift" aria-hidden="true"></i>
 <span>Kuponi</span> 
      <ul <?php if (preg_match("/coupon/i", $url)){ ?> style="display: block;" <?php } ?>>
        <li <?php if (preg_match("/add-coupon/i", $url)){ ?> class="active" <?php } ?>><a href="{{ url('/admin/add-coupon')}}">Dodaj kupon</a></li>
        <li <?php if (preg_match("/view-coupons/i", $url)){ ?> class="active" <?php } ?>><a href="{{ url('/admin/view-coupons')}}">Lista kupona</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i>
<span>Narudžbe</span> 
      <ul <?php if (preg_match("/orders/i", $url)){ ?> style="display: block;" <?php } ?>>
        <li <?php if (preg_match("/view-orders/i", $url)){ ?> class="active" <?php } ?>><a href="{{ url('/admin/view-orders')}}">Pogledaj Narudžbe</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="fa fa-picture-o" aria-hidden="true"></i>
 <span>Slider</span> 
      <ul <?php if (preg_match("/banner/i", $url)){ ?> style="display: block;" <?php } ?>>
        <li <?php if (preg_match("/add-banner/i", $url)){ ?> class="active" <?php } ?>><a href="{{ url('/admin/add-banner')}}">Dodaj novi slide</a></li>
        <li <?php if (preg_match("/view-banners/i", $url)){ ?> class="active" <?php } ?>><a href="{{ url('/admin/view-banners')}}">Lista slika</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="fa fa-user" aria-hidden="true"></i>
 <span>Korisnici</span> 
      <ul <?php if (preg_match("/users/i", $url)){ ?> style="display: block;" <?php } ?>>
        <li <?php if (preg_match("/view-users/i", $url)){ ?> class="active" <?php } ?>><a href="{{ url('/admin/view-users')}}">Lista Korisnika</a></li>
      </ul>
    </li>
  </ul>
</div>
<!--sidebar-menu-->