<footer class="footer-area bg-gray pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-4">
                <div class="copyright mb-30">
                    <div class="footer-logo">
                        <a href="index.html">
                            <img alt="" src="{{asset('/images/frontend_images/logo/logo-2a.png')}}">
                        </a>
                    </div>
                    <p>© 2019 <a href="#">Katriel d.o.o <br> Web Shop</a>.<br> Sva prava pridržana</p>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-4">
                <div class="footer-widget mb-30 ml-30">
                    <div class="footer-title">
                        <h3>O NAMA</h3>
                    </div>
                    <div class="footer-list">
                        <ul>
                            <li><a href="{{ url('/onama') }}">O nama</a></li>
                            <li><a href="{{ url('/pages/kontakt') }}">Kontakt</a></li>
                         
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-2 col-md-6 col-sm-6">
                <div class="footer-widget mb-30 ml-75">
                    <div class="footer-title">
                        <h3>PRATITE NAS</h3>
                    </div>
                    <div class="footer-list">
                        <ul>
                            <li><a href="#">Facebook</a></li>
                         
                        </ul>
                    </div>
                </div>
            </div>
           
        </div>
    </div>
</footer>
