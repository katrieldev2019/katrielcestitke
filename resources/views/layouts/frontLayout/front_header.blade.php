<?php use App\Http\Controllers\Controller;
$mainCategories =  Controller::mainCategories();
?>


<header class="header-area   header-res-padding clearfix">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-2 col-lg-2 col-md-6 col-4">
                <div class="logo">
                    <a href="{{ url('/') }}">
                        <img alt="" src="{{asset('/images/frontend_images/logo/logo-2a.png')}}">
                    </a>
                </div>
            </div>
            <div class="col-xl-8 col-lg-8 d-none d-lg-block">
                <div class="main-menu">
                    <nav>
                    
                        <ul>
                            <li><a href="index.html">Kategorije <i class="fa fa-angle-down"></i></a>
                                <ul class="submenu">
                                    <li><a href="{{ url('/products/rođenje') }}">Rođenje</a></li>
                                    <li><a href="{{ url('/products/vjenčanje') }}">Vjenjčanje</a></li>
                                    <li><a href="{{ url('/products/rođendan') }}">Rođendan</a></li>
                                    <li><a href="{{ url('/products/diploma') }}">Diploma</a></li>
                                    <li><a href="{{ url('/products/krštenje') }}">Krštenje</a></li>
                                    <li><a href="{{ url('/products/sućut') }}">Sućut</a></li>
                                    <li><a href="{{ url('/products/godišnjica') }}">Godišnjica</a></li>
                                    <li><a href="{{ url('/products/mirovina') }}">Mirovina</a></li>
                                </ul>
                            </li>
        
                             
                            
                            
                             <li><a href="index.html">Standardne čestitke <i class="fa fa-angle-down"></i></a>
                                <ul class="submenu">
                                    <li><a href="{{ url('/products/dan-zaljubljenih') }}">Dan zaljubljenih</a></li>
                                    <li><a href="{{ url('/products/rođenje') }}">Rođenje</a></li>
                                    <li><a href="{{ url('/products/vjenčanje') }}">Vjenčanje</a></li>
                                    <li><a href="{{ url('/products/rođendan') }}">Rođendan</a></li>
                                    <li><a href="{{ url('/products/poticaj') }}">Poticaj</a></li>
                                    <li><a href="{{ url('/products/zahvalnost') }}">Zahvalnost</a></li>
                                    <li><a href="{{ url('/products/diploma') }}">Diploma</a></li>
                                    <li><a href="{{ url('/products/krštenje') }}">Krštenje</a></li>
                                    <li><a href="{{ url('/products/krizma') }}">Krizma</a></li>
                                    <li><a href="{{ url('/products/pričest') }}">Pričest</a></li>
                                    <li><a href="{{ url('/products/sućut') }}">Sućut</a></li>
                                </ul>
                            </li>

                            <li><a href="index.html">Jumbo čestitke <i class="fa fa-angle-down"></i></a>
                                <ul class="submenu">
                                    <li><a href="{{ url('/products/vjenčanja-jumbo') }}">Vjenčanja</a></li>
                                    <li><a href="{{ url('/products/rođendan-jumbo') }}">Rođendan</a></li>
                                    <li><a href="{{ url('/products/godišnjica-jumbo') }}">Godišnjica</a></li>
                                    <li><a href="{{ url('/products/putovanje-jumbo') }}">Putovanje</a></li><a href="index-2.html">
                                    </a><li><a href="{{ url('/products/mirovina-jumbo') }}"></a><a href="index-2.html">Mirovina</a></li>
                                </ul>
                            </li>


                            <li><a href="index.html">Pozivnice za vjenčanja <i class="fa fa-angle-down"></i></a>
                                <ul class="submenu">
                                    <li><a href="{{ url('/products/vintage') }}">Vintage</a></li>
                                    <li><a href="{{ url('/products/glam') }}">Glam</a></li>
                                    <li><a href="{{ url('/products/flowers') }}">Flowers</a></li>
                                    <li><a href="{{ url('/products/simple') }}">Simple</a></li>
                                    <li><a href="{{ url('/products/romantic') }}">Romantic</a></li>
                                    <li><a href="{{ url('/products/princess') }}">Princess</a></li>
                                    <li><a href="{{ url('/products/elegant') }}">Elegant</a></li>
                                </ul>
                            </li>

                            <li><a href="#"> <b> + </b><i class="fa fa-angle-down"></i></a>
                                <ul class="submenu">
                                  
                                   
                                    
                                    
                                    @if(empty(Auth::check()))
                                    <li><a href="{{ url('/login-register') }}">login / registracija </a></li>
                                    <li><a href="{{ url('/pages/kontakt') }}">Kontakt</a></li>
								@else
                                    <li><a href="{{ url('/account') }}"> Moj račun</a></li>
                                    <li><a href="{{ url('/cart') }}">Korpa</a></li>
                                    <li><a href="{{ url('/orders') }}"> Narudžbe</a></li>
									<li><a href="{{ url('/user-logout') }}">Odjava</a></li>
								@endif
                                 
                                 
                                </ul>
                            </li>
                           
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-6 col-8">
                   <div class="header-right-wrap">
                    <div class="same-style header-search">
                        <a class="search-active" href="#"><i class="pe-7s-search"></i></a>
                        <div class="search-content">
                        <form action="{{ url('/search-products') }}" method="post">{{ csrf_field() }} 
								<input type="text" placeholder="Search Product" name="product" />
								<button type="submit" style="border:0px; height:33px; margin-left:-3px">Pretraži</button>
							</form>
                        </div> 
                    </div>
               
                    
                    <div class="same-style account-satting">
                        <a href="{{ url('/cart') }}"><i class="pe-7s-cart"></i></a>
                        
                    </div>

                    <div class="same-style account-satting">
                        <a class="account-satting-active" href="#"><i class="pe-7s-user-female"></i></a>
                        <div class="account-dropdown">
                            <ul>
                                    
                            @if(empty(Auth::check()))
                                    <li><a href="{{ url('/login-register') }}">login / registracija </a></li>
								@else
                                    <li><a href="{{ url('/account') }}"> Moj račun</a></li>
                                    <li><a href="{{ url('/cart') }}"> Korpa</a></li>
                                    <li><a href="{{ url('/orders') }}"> Narudžbe</a></li>
									<li><a href="{{ url('/user-logout') }}">Odjava</a></li>
								@endif
                            </ul>
                        </div>
                    </div>
                    
                   
                </div>
            </div>
        </div>
        <div class="mobile-menu-area">
                <div class="mobile-menu">
                    <nav id="mobile-menu-active">
                        <ul class="menu-overflow">
                            <li><a href="index.html">KATEGORIJE</a>
                                <ul>
                                <li><a href="{{ url('/products/rođenje') }}">Rođenje</a></li>
                                    <li><a href="{{ url('/products/vjenčanje') }}">Vjenjčanje</a></li>
                                    <li><a href="{{ url('/products/rođendan') }}">Rođendan</a></li>
                                    <li><a href="{{ url('/products/diploma') }}">Diploma</a></li>
                                    <li><a href="{{ url('/products/krštenje') }}">Krštenje</a></li>
                                    <li><a href="{{ url('/products/sućut') }}">Sućut</a></li>
                                    <li><a href="{{ url('/products/godišnjica') }}">Godišnjica</a></li>
                                    <li><a href="{{ url('/products/mirovina') }}">Mirovina</a></li>
                                </ul>
                            </li>

                           
                            

                            <li><a href="index.html">Standardne čestitke</a>
                                <ul class="submenu">
                                <li><a href="{{ url('/products/dan-zaljubljenih') }}">Dan zaljubljenih</a></li>
                                    <li><a href="{{ url('/products/rođenje') }}">Rođenje</a></li>
                                    <li><a href="{{ url('/products/vjenčanje') }}">Vjenčanje</a></li>
                                    <li><a href="{{ url('/products/rođendan') }}">Rođendan</a></li>
                                    <li><a href="{{ url('/products/poticaj') }}">Poticaj</a></li>
                                    <li><a href="{{ url('/products/zahvalnost') }}">Zahvalnost</a></li>
                                    <li><a href="{{ url('/products/diploma') }}">Diploma</a></li>
                                    <li><a href="{{ url('/products/krštenje') }}">KrštenjeE</a></li>
                                    <li><a href="{{ url('/products/krizma') }}">Krizma</a></li>
                                    <li><a href="{{ url('/products/pričest') }}">Pričest</a></li>
                                    <li><a href="{{ url('/products/sućut') }}">Sućut</a></li>
                                </ul>
                            </li>

                            
                                <li><a href="index-2.html">Jumbo čestitke</a>
                                <ul class="submenu">
                                <li><a href="{{ url('/products/vjenčanja-jumbo') }}">Vjenčanja</a></li>
                                    <li><a href="{{ url('/products/rođendan-jumbo') }}">Rođendan</a></li>
                                    <li><a href="{{ url('/products/godišnjica-jumbo') }}">Godišnjica</a></li>
                                    <li><a href="{{ url('/products/putovanje-jumbo') }}">Putovanje</a></li><a href="index-2.html">
                                    </a><li><a href="{{ url('/products/mirovina-jumbo') }}"></a><a href="index-2.html">Mirovina</a></li>
                                  
                                </ul>
                            </li>


                            <li><a href="index.html">Pozivnice za vjenčanja</a>
                                <ul class="submenu">
                                <li><a href="{{ url('/products/vintage') }}">Vintage</a></li>
                                    <li><a href="{{ url('/products/glam') }}">Glam</a></li>
                                    <li><a href="{{ url('/products/flowers') }}">Flowers</a></li>
                                    <li><a href="{{ url('/products/simple') }}">Simple</a></li>
                                    <li><a href="{{ url('/products/romantic') }}">Romantic</a></li>
                                    <li><a href="{{ url('/products/princess') }}">Princess</a></li>
                                    <li><a href="{{ url('/products/elegant') }}">Elegant</a></li>
                                </ul>
                            </li>

                            <li><a href="#"> <b> + </b></a>
                                <ul class="submenu">
                               
                                    <li><a href="cart-page.html">Košarica</a></li>
                                    <li><a href="checkout.html">Izlaz </a></li>
                                    <li><a href="wishlist.html">Lista želja</a></li>
                                    <li><a href="my-account.html">Moj račun</a></li>
                                    <li><a href="{{ url('/login-register') }}">login / registracija </a></li>
                                   
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
    </div>
</header>