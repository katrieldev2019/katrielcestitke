<div class="left-sidebar">
				<div class="mt-3"></div>
					<h2>Kategorije</h2>
					<div class="mt-5"></div>
						<div class="category-products" id="accordian">
							<!--category-products-->
							@foreach($categories as $cat)
							<div class="card  card-default">
								<div class="card-heading">
									<h4 class="card-title">
										<a class="sidebarA"  data-toggle="collapse" data-parent="#accordian" href="#{{$cat->id}}">
											<span class="badge "><i class="fa fa-plus"></i></span>
											{{$cat->name}}
										</a>
									</h4>
                                </div>
                             
								<div id="{{$cat->id}}" class="panel-collapse collapse">
									<div class="card-body">
										<ul>
											@foreach($cat->categories as $subcat)
												@if($subcat->status==1)
												<li><a  class="sidebarCad" href="{{ asset('products/'.$subcat->url) }}">{{$subcat->name}} </a></li>
												@endif
											@endforeach
										</ul>
									</div>
								</div>
                            </div>
                           <div class="dividerA"></div>
							@endforeach

						</div><!--/category-products-->
					</div>