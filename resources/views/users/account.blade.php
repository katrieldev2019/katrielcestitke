@extends('layouts.frontLayout.front_design')
@section('content')
<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <ul>
                <li>
                    <a href="index.html">Početna</a>
                </li>
                <li class="active">Moj račun </li>
            </ul>
        </div>
    </div>
</div>
<section id="form" style="margin-top:20px;"><!--form-->

	<div class="container">
		<div class="row">
			@if(Session::has('flash_message_success'))
	            <div class="alert alert-success alert-block">
	                <button type="button" class="close" data-dismiss="alert">×</button> 
	                    <strong>{!! session('flash_message_success') !!}</strong>
	            </div>
	        @endif
	        @if(Session::has('flash_message_error'))
	            <div class="alert alert-error alert-block" style="background-color:#f4d2d2">
	                <button type="button" class="close" data-dismiss="alert">×</button> 
	                    <strong>{!! session('flash_message_error') !!}</strong>
	            </div>
			@endif  
			




			<div id="my-account-1" class="panel-collapse collapse show">
                                <div class="panel-body">
                                    <div class="myaccount-info-wrapper">
                                        <div class="account-info-wrapper">
                                            <h4>Informacije o mom računu</h4>
										</div>
										<form id="accountForm" name="accountForm" action="{{ url('/account') }}" method="POST">{{ csrf_field() }}

                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="billing-info">
                                                    <label>Ime i Przime</label>
                                                    <input value="{{ $userDetails->name }}" id="name" name="name" type="text" />
                                                </div>
                                            </div>
                                          
                                            <div class="col-lg-6 col-md-6">
                                                <div class="billing-info">
                                                    <label>Email Addresa</label>
                                                    <input type="email" value="{{ $userDetails->email }}" readonly=""/>
                                                </div>
											</div>
											<div class="col-lg-6 col-md-6">
                                                <div class="billing-info">
                                                    <label>Adresa</label>
                                                    <input value="{{ $userDetails->address }}" id="address" name="address" type="text" />
                                                </div>
											</div>
											
                                            <div class="col-lg-6 col-md-6">
                                                <div class="billing-info">
                                                    <label>Telefon</label>
                                                    <input value="{{ $userDetails->mobile }}" id="mobile" name="mobile" type="text" />
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="billing-info">
                                                    <label>Grad</label>
                                                    <input value="{{ $userDetails->city }}" id="city" name="city" type="text" />
                                                </div>
											</div>
											<div class="col-lg-6 col-md-6">
                                                <div class="billing-info">
                                                    <label>Kanton</label>
                                                    <input value="{{ $userDetails->state }}" id="state" name="state" type="text" />
                                                </div>
											</div>
											<div class="col-lg-6 col-md-6">
                                                <div class="billing-info">
													<label>Država</label>
													<div class="py-1"></div>
													<select id="country" name="country">
													<option value="">Odaberi državu</option>
													@foreach($countries as $country)
														<option value="{{ $country->country_name }}" @if($country->country_name == $userDetails->country) selected @endif>{{ $country->country_name }}</option>
													@endforeach
												</select>
                                                </div>
											</div>
											<div class="col-lg-6 col-md-6">
                                                <div class="billing-info">
                                                    <label>Poštanski broj</label>
                                                    <input value="{{ $userDetails->pincode }}" style="margin-top: 10px;" id="pincode" name="pincode" type="text" />
                                                </div>
											</div>
                                        </div>
                                        <div class="billing-back-btn">
                                            
										<div class="py-2"></div>
											<button type="submit" class="btnCustom">Ažuriraj</button>
											
										</form>
										<div class="py-2"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

				<br/>
				<br/>
				<br/>
			<div class="col-sm-4">
				
			<br/>
				<br/>
				<br/>
				<div class="signup-form">
					<h2>Ažurirajte lozinku</h2>
					<form id="passwordForm" name="passwordForm" action="{{ url('/update-user-pwd') }}" method="POST">{{ @csrf_field() }}
					<label>Trenutna lozinka</label>
					<input type="password" name="current_pwd" id="current_pwd" >
						<div class="py-2"></div>
						<span id="chkPwd"></span>
						<label>Nova lozinka</label>
						<input type="password" name="new_pwd" id="new_pwd" >
						<div class="py-2"></div>
						<label>Potvrdi lozinku</label>
						<input type="password" name="confirm_pwd" id="confirm_pwd" >
						<br/>
				<br/>
						<button type="submit" class="btnCustom">Ažuriraj lozinku</button>
						<div class="py-2"></div>
						<div class="py-2"></div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section><!--/form-->

@endsection