@extends('layouts.frontLayout.front_design')
@section('content')
<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <ul>
                <li>
                    <a href="index.html">Početna</a>
                </li>
                <li class="active">login/Registracija </li>
            </ul>
        </div>
    </div>
</div>
<div class="login-register-area pt-100 pb-100">
    <div class="container">
	@if(Session::has('flash_message_success'))
	            <div class="alert alert-success alert-block">
	                <button type="button" class="close" data-dismiss="alert">×</button> 
	                    <strong>{!! session('flash_message_success') !!}</strong>
	            </div>
	        @endif
	        @if(Session::has('flash_message_error'))
	            <div class="alert alert-error alert-block" style="background-color:#f4d2d2">
	                <button type="button" class="close" data-dismiss="alert">×</button> 
	                    <strong>{!! session('flash_message_error') !!}</strong>
	            </div>
            @endif  
            
    
            <div class="row">
                    <div class="col-md-6">
                    <div class="login-register-wrapper">
                    <h2>Prijava</h2>
                    <form id="loginForm" name="loginForm" action="{{ url('/user-login') }}" method="POST">{{ csrf_field() }}
                                       
                    <input name="email" type="email" placeholder="Email Address" />
                                       <div class="py-3"></div>
                                        <input name="password" type="password" placeholder="Password" />
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>
                                        <div class="button-box">
                                              
                                                <button type="submit" class="btn btn-danger">Prijavi se</button>
</div>
                    </form>
                    </div>
                    </div>

                    <div class="col-md-6">
                    <div class="login-register-wrapper">
                    <h2>Registracija</h2>
                    <form id="registerForm" name="registerForm" action="{{ url('/user-register') }}" method="POST">{{ csrf_field() }}
                                            <input id="name" name="name" type="text" placeholder="Name"/>
                                            <div class="py-3"></div>
                                            <input id="email" name="email" type="email" placeholder="Email Address"/>
                                            <div class="py-3"></div>
                                            <input id="myPassword" name="password" type="password" placeholder="Password"/>
                                            <div class="py-3"></div>
                                            <button type="submit" class="btn btn-danger">Registriraj se</button>
                    </form>
                </div>
                    </div>


            </div>
      
    </div>
</div>


@endsection