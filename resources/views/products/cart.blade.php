@extends('layouts.frontLayout.front_design')
@section('content')

<section id="cart_items">
<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
				<div class="container">
					<div class="breadcrumb-content text-center">
						<ul>
							<li>
								<a href="index.html">Početna</a>
							</li>
							<li class="active">Korpa </li>
						</ul>
					</div>
				</div>
			</div>
		<div class="container">
		
			<div class="py-3"></div>
			<div class="table-responsive cart_info">
				@if(Session::has('flash_message_success'))
		            <div class="alert alert-success alert-block">
		                <button type="button" class="close" data-dismiss="alert">×</button> 
		                    <strong>{!! session('flash_message_success') !!}</strong>
		            </div>
		        @endif
		        @if(Session::has('flash_message_error'))
		            <div class="alert alert-error alert-block" style="background-color:#f4d2d2">
		                <button type="button" class="close" data-dismiss="alert">×</button> 
		                    <strong>{!! session('flash_message_error') !!}</strong>
		            </div>
				@endif   
				<div class="table-content table-responsive cart-table-content">
                        <table>
                            <thead>
                                <tr>
                                    <th>Foto</th>
                                    <th>Naziv artikla</th>
                                    <th>Cijena</th>
                                    <th>Količina</th>
                                    <th>Suma stavke</th>
                                    <th>Akcija</th>
                                </tr>
                            </thead>
                            <tbody>
							
							<?php $total_amount = 0; ?>
						@foreach($userCart as $cart)
							<tr>
								<td class="product-thumbnail">
									<a href=""><img style="width:100px;" src="{{ asset('/images/backend_images/product/small/'.$cart->image) }}" alt=""></a>
								</td>
								<td class="product-name">
									<h4><a href="">{{ $cart->product_name }}</a></h4>
									<p>Kod artikla: {{ $cart->product_code }}</p>
								</td>
								<td class="product-price-cart">
									<p> {{ $cart->price }} KM</p>
								</td>
								<td class="cart_quantity">
									<div class="product-quantity">
										<a class="cart_quantity_up" href="{{ url('/cart/update-quantity/'.$cart->id.'/1') }}"> + </a>
										<input class="cart_quantity_input" type="text" name="quantity" value="{{ $cart->quantity }}" autocomplete="off" size="2">
										@if($cart->quantity>1)
											<a class="cart_quantity_down" href="{{ url('/cart/update-quantity/'.$cart->id.'/-1') }}"> - </a>
										@endif
									</div>
								</td>
								<td class="product-subtotal">
									<p class="cart_total_price">KM {{ $cart->price*$cart->quantity }}</p>
								</td>
								<td class="cart_delete">
									<a class="cart_quantity_delete" href="{{ url('/cart/delete-product/'.$cart->id) }}"><i class="fa fa-times"></i></a>
								</td>
							</tr>
							<?php $total_amount = $total_amount + ($cart->price*$cart->quantity); ?>
						@endforeach
                               
                               
                            </tbody>
                        </table>
                    </div>
				
			</div>
		</div>
</section> <!--/#cart_items-->

<section id="do_action">
	<div class="container">
		<div class="heading">
			<div class="py-5"></div>
			<h3>Što želite da uradite sljedeće?</h3>
			<p>Unesite kupon kod ako ga posjedujte.</p>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="chose_area">
					<ul class="user_option">
						<li>
							<form action="{{ url('cart/apply-coupon') }}" method="post">{{ csrf_field() }}
								<label>Kupon kod</label>
							
								<input class="cart_quantity_input" type="text"  name="coupon_code"  " autocomplete="off" size="2">
								<div class="cart-shiping-update-wrapper">
										<div class="cart-shiping-update">
											<a href="#">Aktiviraj kupon</a>
										</div>
								</div>
							</form>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-lg-4 col-md-12">
                        <div class="grand-totall">
                            <div class="title-wrap">
                                <h4 class="cart-bottom-title section-bg-gary-cart">Košarica Ukupno</h4>
                            </div>
                           
                            <div class="total-shipping">
                               
								<ul>
									@if(!empty(Session::get('CouponAmount')))
										<li>Sub Total <span>KM <?php echo $total_amount; ?></span></li>
										<li>Coupon Discount <span>KM <?php echo Session::get('CouponAmount'); ?></span></li>
										<li>Grand Total <span>KM <?php echo $total_amount - Session::get('CouponAmount'); ?></span></li>
									@else
										<li>Grand Total <span>KM <?php echo $total_amount; ?></span></li>
										<h4 class="grand-totall-title">Total  <span><?php echo $total_amount; ?></span></h4>
									@endif
								</ul>
                            </div>
                            <a href="{{url('/checkout')}}">Sljedeći korak</a>
                        </div>
                    </div>
			
		</div>
		<div class="py-3"></div>
	</div>
</section><!--/#do_action-->

@endsection