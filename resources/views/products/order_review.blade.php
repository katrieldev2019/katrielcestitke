@extends('layouts.frontLayout.front_design')
@section('content')
<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
				<div class="container">
					<div class="breadcrumb-content text-center">
						<ul>
							<li>
								<a href="index.html">Početna</a>
							</li>
							<li class="active">Recenzija narudžbe</li>
						</ul>
					</div>
				</div>
</div>
<div class="py-5"></div>
<section id="cart_items">
		<div class="container">
			

			<div class="shopper-informations">
				<div class="row">					
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form">
						<h2>Podaci za račun</h2>
							<div class="form-group">
							<b>Ime i Prezime:</b> {{ $userDetails->name }}
							</div>
							<div class="form-group">
							<b>Adresa: </b>	{{ $userDetails->address }}
							</div>
							<div class="form-group">	
							<b>Grad: </b>{{ $userDetails->city }}
							</div>
							<div class="form-group">
							<b>Kanton: </b>	{{ $userDetails->state }}
							</div>
							<div class="form-group">
							<b>Država: </b>	{{ $userDetails->country }}
							</div>
							<div class="form-group">
							<b>Poštanski broj:</b>{{ $userDetails->pincode }}
							</div>
							<div class="form-group">
							<b>Broj telefona:</b>	{{ $userDetails->mobile }}
							</div>
					</div>
				</div>
				<div class="col-sm-1">
					<h2></h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form">
						<h2>Podaci za dostavu</h2>
							<div class="form-group">
								<b>Ime i Prezime:</b> {{ $shippingDetails->name }}
							</div>
							<div class="form-group">
							<b>Adresa: </b>	{{ $shippingDetails->address }}
							</div>
							<div class="form-group">	
							<b>Grad: </b>	{{ $shippingDetails->city }}
							</div>
							<div class="form-group">
							<b>Kanton: </b>	{{ $shippingDetails->state }}
							</div>
							<div class="form-group">
							<b>Država: </b>	{{ $shippingDetails->country }}
							</div>
							<div class="form-group">
							<b>Poštanski broj:</b>	{{ $shippingDetails->pincode }}
							</div>
							<div class="form-group">
							<b>Broj telefona:</b>	{{ $shippingDetails->mobile }}
							</div>
					</div>
				</div>
			</div>
			<div class="py-5"></div>

			<div class="review-payment">
				<h2>Recenzija i plačanje</h2>
			</div>

			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Proizvod</td>
							<td class="description"></td>
							<td class="price">Cijena</td>
							<td class="quantity">Količina</td>
							<td class="total">Ukupno</td>
						</tr>
					</thead>
					<tbody>
						<?php $total_amount = 0; ?>
						@foreach($userCart as $cart)
						<tr>
							<td class="cart_product">
								<a href=""><img style="width:130px;" src="{{ asset('/images/backend_images/product/small/'.$cart->image) }}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{ $cart->product_name }}</a></h4>
								<p>Kod proizvoda: {{ $cart->product_code }}</p>
							</td>
							<td class="cart_price">
								<p>{{ $cart->price }} KM</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									{{ $cart->quantity }}
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price"> {{ $cart->price*$cart->quantity }} KM</p>
							</td>
						</tr>
						<?php $total_amount = $total_amount + ($cart->price*$cart->quantity); ?>
						@endforeach
						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Korpa subtotal</td>
										<td> {{ $total_amount }} KM</td>
									</tr>
									
									<tr class="shipping-cost">
										<td>Kupon popust (-)</td>
										<td>
											@if(!empty(Session::get('CouponAmount')))
												 {{ Session::get('CouponAmount') }} KM
											@else
												 0 KM
											@endif
										</td>	
									</tr>
									<tr>
										<td>Ukupno</td>
										<td><span> {{ $grand_total = $total_amount - Session::get('CouponAmount') }} KM</span></td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<form name="paymentForm" id="paymentForm" action="{{ url('/place-order') }}" method="post">{{ csrf_field() }}
				<input type="hidden" name="grand_total" value="{{ $grand_total }}">
				<div >
					
					<span>
						<label><strong>Odaberi metodu plačanja:</strong></label>
						<label><input type="radio" name="payment_method" id="COD" value="COD"/> <strong>COD</strong></label>

					</span>
					
					
					
					<span style="float:right;">
						<button type="submit" class="btn btn-default" onclick="return selectPaymentMethod();">Završi narudžbu</button>
					</span>
				</div>
			</form>
		</div>
	</section> <!--/#cart_items-->

@endsection