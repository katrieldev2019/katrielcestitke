@extends('layouts.frontLayout.front_design')

@section('content')

<section>
<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <ul>
                <li>
                    <a href="index.html">Početna</a>
                </li>
                <li class="active">Artikal detaljno</li>
            </ul>
        </div>
    </div>
</div>
		<div class="container">
			<div class="row">
			@if(Session::has('flash_message_error'))
	            <div class="alert alert-error alert-block" style="background-color:#d7efe5">
	                <button type="button" class="close" data-dismiss="alert">×</button> 
	                    <strong>{!! session('flash_message_error') !!}</strong>
	            </div>
	        @endif   
			<div class="shop-area pt-100 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="product-details">
                    <div class="product-details-img">
                        <div class="tab-content jump"> 
                        <div id="shop-details-2" class="tab-pane active large-img-style">
                                <img src="{{ asset('/images/backend_images/product/medium/'.$productDetails->image) }}" alt="">
                                
                               
                            </div>

                            
                           
                            
                        </div>
                       
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="product-details-content ml-70">
                    <h2>{{ $productDetails->product_name }}</h2>
                    <div class="mt-2"></div>
              
                    <div class="product-details-price">
                        <span>{{   number_format( $productDetails->price, 2) }} KM</span>
                       
                    </div>
                    
                    <p>{{ $productDetails->description }}</p>
                   
                   
                    <div class="pro-details-quality">
					<form name="addtoCartForm" id="addtoCartForm" action="{{ url('add-cart') }}" method="post">{{ csrf_field() }}
					<input type="hidden" name="product_id" value="{{ $productDetails->id }}">
		                        <input type="hidden" name="product_name" value="{{ $productDetails->product_name }}">
		                        <input type="hidden" name="product_code" value="{{ $productDetails->product_code }}">
		                        <input type="hidden" name="product_color" value="{{ $productDetails->product_color }}">
		                        <input type="hidden" name="price" id="price" value="{{ $productDetails->price }}">
								<div class="pro-details-quality">
                        <div class="cart-plus-minus">
                            <input name="quantity" class="cart-plus-minus-box" type="text"  value="1">
						</div>
						
                        <div class="pro-details-cart btn-hover">
						<button type="submit" class="btnCustom" id="cartButton">
											
												Dodaj u korpu
											</button>
						</div>
						
                        
                       
                    </div>
                       
						
									
									
                       
					</div>
				
                   
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>

   





















				

			</div>
		</div>
	</section>	

@endsection