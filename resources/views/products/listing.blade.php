@extends('layouts.frontLayout.front_design')

@section('content')


<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <ul>
                <li>
                    <a href="index.html">Početna</a>
                </li>
                <li class="active">Shop </li>
            </ul>
        </div>
    </div>
</div>
<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				@include('layouts.frontLayout.front_sidebar')
			</div>
			
			<div class="col-sm-9 padding-right">
				<div class="features_items"><!--features_items-->
					

					<div class="shop-bottom-area mt-35">
                    <div class="tab-content jump">
                        <div id="shop-1" class="tab-pane active">
                            <div class="row">
								@foreach($productsAll as $pro)
                                <div class="product-wrap mb-25 mr-5 scroll-zoom">
                                        <div class="product-img">
                                            <a href="#">
                                                <img  height="345" width="270" class="default-img" src="{{ asset('/images/backend_images/product/small/'.$pro->image) }}" alt="">
                                               
                                            </a>
                                            
                                            <div class="product-action">
                                                
                                                <div class="pro-same-action pro-cart">
                                                    <a  style="text-decoration:none; "  title="Add To Cart" href="{{ url('/product/'.$pro->id) }}" ><i class="pe-7s-cart"></i> Dodaj u korpu</a>
                                                </div>
                                               
                                            </div>
                                        </div>
                                        <div class="product-content text-center">
                                            <h3><a href="{{  url('/product/'.$pro->id) }}">{{ $pro->product_name }}</a></h3>
                                           
                                            <div class="product-price">
                                                <span>{{ $pro->price }} KM</span>
                                                
                                            </div>
                                        </div>
                                    </div>
                             
                             
								@endforeach
                             
                                
                               
                                
                               
                               
                            </div>
                        </div>
                      
                    </div>
                    <div class="pro-pagination-style text-center mt-30">
                        <ul>
                            <li><a class="prev" href="#"><i class="fa fa-angle-double-left"></i></a></li>
                            <li><a class="active" href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a class="next" href="#"><i class="fa fa-angle-double-right"></i></a></li>
                        </ul>
                    </div>
                </div>








































					
				</div><!--features_items-->
				
			</div>
		</div>
	</div>
</section>

@endsection