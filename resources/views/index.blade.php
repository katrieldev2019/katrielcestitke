@extends('layouts.frontLayout.front_design')

@section('content')
<div class="single-slider-2 slider-height-1 d-flex align-items-center slider-height-res bg-img" style="background-image:url(/images/frontend_images/slider/slider-2.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-7 ml-auto">
                        <div class="slider-content-2 slider-animated-1">
                            <h3 class="animated"><b>SNIŽENJE</b></h3>
                            <h1 class="animated ml-5">Za sve mladence <br>20% sniženja</h1>
                            <div class="slider-btn btn-hover">
                                <a class="animated" href="shop-list-fw.html">SAZNAJ VIŠE</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<div class="slider-area">

    <div class="slider-active owl-carousel nav-style-1">
        <div class="single-slider-2 slider-height-1 d-flex align-items-center slider-height-res bg-img" id="slider1">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-7 ml-auto">
                        <div class="slider-content-2 slider-animated-1">
                            <h3 class="animated"><b>SNIŽENJE</b></h3>
                            <h4 class="animated">Za sve mladence <br>20% sniženja</h4>
                            <div class="slider-btn btn-hover">
                                <a class="animated" href="shop-list-fw.html">SAZNAJ VIŠE</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="single-slider-2 slider-height-1 d-flex align-items-center slider-height-res bg-img" style="background-image:url(assets/img/slider/slider-2.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-7 ml-auto">
                        <div class="slider-content-2 slider-animated-1">
                        
                            <h3 class="animated"><b>PONUDA</b></h3>
                            <h1 class="animated ml-5">Veliki izbor čestitki za sve okolnosti </h1>
                            <div class="slider-btn btn-hover">
                                <a class="animated" href="shop-list-fw.html">SAZNAJ VIŠE</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="single-slider-2 slider-height-1 d-flex align-items-center slider-height-res bg-img" style="background-image:url(assets/img/slider/slider-3.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-7 ml-auto">
                        <div class="slider-content-2 slider-animated-1">
                            <h3 class="animated"><b>PONUDA</b></h3>
                            <h4 class="animated">Veliki izbor čestitki za sve okolnosti </h4>
                            <div class="slider-btn btn-hover">
                                <a class="animated" href="shop-list-fw.html">SAZNAJ VIŠE</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="suppoer-area pt-100 pb-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="support-wrap mb-30 support-1">
                    <div class="support-icon">
                        <img class="animated" src="{{asset('/images/frontend_images/icon-img/support-1.png')}}" alt="">
                    </div>
                    <div class="support-content">
                        <h5 class="py-3">Brza Dostava</h5>
                       
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="support-wrap mb-30 support-2">
                    <div class="support-icon">
                        <img class="animated" src="{{asset('/images/frontend_images/icon-img/support-2.png')}}" alt="">
                    </div>
                    <div class="support-content">
                        <h5 class="py-3">Podrška 24/7</h5>
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="support-wrap mb-30 support-3">
                    <div class="support-icon">
                        <img class="animated" src="{{asset('/images/frontend_images/icon-img/support-3.png')}}" alt="">
                    </div>
                    <div class="support-content">
                        <h5 class="py-3">Povrat novca</h5>
                       
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="support-wrap mb-30 support-4">
                    <div class="support-icon">
                        <img class="animated" src="{{asset('/images/frontend_images/icon-img/support-4.png')}}" alt="">
                    </div>
                    <div class="support-content">
                        <h5 class="py-3">Popust na količinu</h5>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="banner-area pt-60 pb-65">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="single-banner mb-30">
                    <a href="{{ url('/products/rođenje') }}"><img src="{{asset('/images/frontend_images/banner/banner-1.jpg')}}" alt=""></a>
                    <div class="banner-content">
					  
					
                        <a href="{{ url('/products/rođenje') }}"><i class="fas fa-arrow-right"></i></a>
					</div>
					<h3>Čestitke za rođenje</h3>
                        <h6>Prigodne čestitke za rođenje djeteta, u raznim dimenzijama</h6>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="single-banner mb-30">
				<a href="{{ url('/products/vjenčanje') }}"><img src="{{asset('/images/frontend_images/banner/banner-1.jpg')}}" alt=""></a>
                    <div class="banner-content">
                       
                        <a href="{{ url('/products/vjenčanje') }}"><i class="fas fa-arrow-right"></i></a>
					</div>
					<h3>Čestitke za vjenjčanje</h3>
                        <h6>Prigodne čestitke za vjenčanje, u raznim dimenzijama...</h6>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="single-banner mb-30">
                    <a href="{{ url('/products/rođendan') }}"><img src="{{asset('/images/frontend_images/banner/banner-3.jpg')}}" alt=""></a>
                    <div class="banner-content">
                       
                        <a href="{{ url('/products/rođendan') }}"><i class="fas fa-arrow-right"></i></a>
					</div>
					<h3>Čestitke za rođendane</h3>
                        <h6>Prigodne čestitke za rođendan, u raznim dimenzijama...</h6>
                </div>
            </div>
        </div>
    </div>
</div>



            </div>
        </div>
<div class="mt-5"></div>
    </div>
    
    <div class="container">
    <div class="tab-filter-wrap mb-60">
            <div class="product-tab-list-2 nav">
                <a href="#product-1" data-toggle="tab">
                    <h4>Novi artikli  </h4>
                    <hr/>
                </a>
               
            </div>
          
    </div>
         <div class="tab-pane active" id="product-2">
                            <div class="row">
                            @foreach($recProduct as $pro)
                                <div class="col-xl-3 col-md-6 col-lg-4 col-sm-6">
                                    <div class="product-wrap-2 mb-25 scroll-zoom">
                                        <div class="product-img">
                                            <a href="product-details.html">
                                                <img class="default-img" src="{{ asset('/images/backend_images/product/small/'.$pro->image) }}" alt="">
                                                <img class="hover-img" src="{{ asset('/images/backend_images/product/small/'.$pro->image) }}" alt="">
                                            </a>
                                            <div class="product-action-2">
                                                <a title="Add To Cart" href="{{ url('/product/'.$pro->id) }}"><i class="fa fa-shopping-cart"></i></a>
                                            </div>
                                        </div>
                                        <div class="product-content-2">
                                            <div class="title-price-wrap-2">
                                                <h3><a href="{{ url('/product/'.$pro->id) }}" >{{ $pro->product_name }}</a></h3>
                                                <div class="price-2">
                                                    <span>{{ $pro->price }} KM</span>
                                                </div>
                                            </div>
                                        
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
</div>
@endsection