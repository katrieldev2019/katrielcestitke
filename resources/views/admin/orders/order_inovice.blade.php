<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">
        <div class="col-xs-12">
    		<div class="invoice-title">
			<img class="pull-right " style="margin-top:40px;" src="{{ asset('images/frontend_images/home/logo.png') }}" height="26" width="150" alt="" />
			<br>
			<br>
			<br>
			<br>
			<br>
			<div class="pull-right">
				<p><b>Katriel d.o.o</b></p>
				<p> Donje Putićevo bb, <br>Travnik 72276 </p>
			</div>
    		</div>
			
			
    		<div class="row">
    			<div class="col-xs-6">
					<p>Donje Putićevo bb, Travnik 72276</p>

					<hr/>
    				<address>
    			
                    {{ $userDetails->name }} <br>
                {{ $userDetails->address }} <br>
                {{ $userDetails->state }} <br>
                {{ $userDetails->country }} <br>
                {{ $userDetails->pincode }} <br>
                {{ $userDetails->mobile }} <br>
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
        		<!--	<strong>Shipped To:</strong><br>
                    //{{ $orderDetails->name }} <br>
                {{ $orderDetails->address }} <br>
                {{ $orderDetails->state }} <br>
                {{ $orderDetails->country }} <br>
                {{ $orderDetails->pincode }} <br>
                {{ $orderDetails->mobile }} <br>-->
    				</address>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-xs-12">
    				<address>
    					<strong>Račun -:{{ $orderDetails->id }}</strong><br>
    					<hr/>
    				</address>
    			</div>
				<div class="col-xs-12 ">
					<h4> Kupovina</h4>
    				<address>
    					<strong>Datum kupnje:</strong>
						{{ $orderDetails->created_at }}<br>
					
					</address>
					<hr/>
					
    			</div>
    		</div>
    	</div>
    </div>

    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Sažetak narudžbe</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>Kod proizvoda</strong></td>
                                    <td class="text-center"><strong>Naziv proizvoda</strong></td>
                                    <td class="text-center"><strong>Cijena proizvoda</strong></td>
        							<td class="text-center"><strong>Količina</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    							<!-- foreach ($order->lineItems as $line) or some such thing here -->
                                @foreach($orderDetails->orders as $pro)
    							<tr>
                                <td>{{ $pro->product_code }}</td>
                                <td>{{ $pro->product_name }}</td>
                                <td>{{ $pro->product_price }}</td>
                                <td>{{ $pro->product_qty }}</td>
    							</tr>
                                @endforeach
								<?php
 
								//The VAT rate / percentage.
								$vat = 7.7;
								
								//The price, excluding VAT.
								$priceExcludingVat = $orderDetails->grand_total;
								
								//Calculate how much VAT needs to be paid.
								$vatToPay = ($priceExcludingVat / 100) * $vat;
								
								//The total price, including VAT.
								$totalPrice = $priceExcludingVat + $vatToPay;
								?>
    							<tr>
    								<td class="thick-line"></td>
    								<td class="thick-line"></td>
    								<td class="thick-line text-center"><strong>Subtotal</strong></td>
    								<td class="thick-line text-right"> {{$totalPrice = $priceExcludingVat + $vatToPay}} KM</td>
    							</tr>
    						
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Total</strong></td>
    								<td class="no-line text-right"> {{ $orderDetails->grand_total }} KM</td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>