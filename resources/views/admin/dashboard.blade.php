@extends('layouts.adminLayout.admin_design')
@section('content')
<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Admin kontrolna ploča</a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
    <div class="quick-actions_homepage">
      <ul class="quick-actions">
        <li class="bg_lb span3"> <a href="/admin/view-users"> <i class="icon-user"></i>  Korisnici<br/>  {{ $userCount}}</a> </li>
        <li class="bg_lg span3"> <a href="/admin/view-products"> <i class="icon-signal"></i> Proizvodi<br/> {{$productsCount}} </a> </li>
        <li class="bg_ly span2"> <a href="/admin/view-orders"> <i class="icon-inbox"></i> Naruđžbe<br/>{{$ordersCount}} </a> </li>
        <li class="bg_lo span2"> <a href="/admin/view-categories"> <i class="icon-th"></i> Kategorije<br/>{{$categoriesCount}}</a> </li>
      

      </ul>
    </div>
<!--End-Action boxes-->    

<!--Chart-box-->    
    <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>
          <h5>Zadnje narudžbe</h5>
        </div>
        <div class="widget-content" >
          <div class="row-fluid">
          
          <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>ID narudžbe</th>
                  <th>Datum narudžbe</th>
                  <th>Ime kupca</th>
                  <th>E-pošta korisnika</th>
                  <th>Naručeni proizvodi</th>
                  <th>Iznos narudžbe</th>
                  <th>Stanje narudžbe</th>
                  <th>Način plaćanja</th>
                  <th>akcije</th>
                </tr>
              </thead>
              <tbody>
              	@foreach($orders as $order)
                <tr class="gradeX">
                  <td class="center">{{ $order->id }}</td>
                  <td class="center">{{ $order->created_at }}</td>
                  <td class="center">{{ $order->name }}</td>
                  <td class="center">{{ $order->user_email }}</td>
                  <td class="center">
                    @foreach($order->orders as $pro)
                    {{ $pro->product_code }}
                    ({{ $pro->product_qty }})
                    <br>
                    @endforeach
                  </td>
                  <td class="center">{{ $order->grand_total }}</td>
                  <td class="center">{{ $order->order_status }}</td>
                  <td class="center">{{ $order->payment_method }}</td>
                  <td class="center">
                    <a target="_blank" href="{{ url('admin/view-order/'.$order->id)}}" class="btn btn-success btn-mini">Prikaz pojedinosti narudžbe</a>
                    <a target="_blank" href="{{ url('admin/view-order-inovice/'.$order->id)}}" class="btn btn-success btn-mini">Prikaz računa narudžbe</a>  
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
           
          </div>
        </div>
      </div>
    </div>
<!--End-Chart-box--> 
    <hr/>
    
  </div>
</div>

<!--end-main-container-part-->

@endsection